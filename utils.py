def parse_file(filename):
	blocks_by_line = []
	blocks_by_column = []

	with open(filename, 'r') as f:
		for l in f: 
			l = l.strip()
			if l == '#': break
			blocks_by_line.append(tuple(int(i) for i in l.split()))
		for l in f:
			blocks_by_column.append(tuple(int(i) for i in l.strip().split()))

	return tuple(blocks_by_line), tuple(blocks_by_column)