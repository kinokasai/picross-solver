from gurobipy import Model, GRB, quicksum
from utils import parse_file
from pylab import figure, imshow, savefig, grid
from sys import argv


def set_x_vars(m, nl, nc):
	"""Create one x_var per cell of the grid.

	Args:
		m (Model): gurobi Model class.
		nl (int): number of lines.
		nc (int): number of columns.

	Return: (dict) all variables indexed by their position in the grid.
	"""
	return {(i, j): m.addVar(name='x[{},{}]'.format(i,j), vtype=GRB.BINARY) 
			for i in range(nl) for j in range(nc)}


def set_y_vars(m, nc, l_blocks):
	"""Create y variables for each position that each block could take in the 
	lines.

	Args:
		m (Model): gurobi Model class.
		nc (int): number of columns.
		l_blocks (list list int): list of blocks for each line.

	Return: (dict) all variables indexed by their position in the grid and by
		their corresponding block number.
	"""
	y_vars = {}

	for i, blocs in enumerate(l_blocks):
		for t, bloc in enumerate(blocs):

			binf = sum(blocs[:t]) + len(blocs[:t])
			bsup = nc - sum(blocs[t:]) - len(blocs[t+1:])

			for j in range(binf, bsup+1):
				y_vars[(i,j,t+1)] = m.addVar(vtype=GRB.BINARY, 
					name="y[{},{},{}]".format(i,j,t+1))
				
	return y_vars

			
def set_y_constrs(m, nc, l_blocks, y_vars, x_vars):
	"""Set the constraints for each y_var.

	Args:
		m (Model): gurobi Model class.
		nc (int): number of columns.
		l_blocks (list list int): list of blocks for each line.
		y_vars (dict): collection of gurobi vars involved in the constraints.
		x_vars (dict): collection of gurobi vars involved in the constraints.
	"""

	for i, blocs in enumerate(l_blocks):
		for t, bloc in enumerate(blocs):
			binf1 = sum(blocs[:t]) + len(blocs[:t])
			bsup1 = nc - sum(blocs[t:]) - len(blocs[t+1:])

			m.addConstr(quicksum(y_vars[(i,j,t+1)] 
				for j in range(binf1, bsup1+1)) == 1)

			for j in range(binf1, bsup1+1):
				m.addConstr(bloc * y_vars[(i,j,t+1)] <= quicksum(x_vars[(i,j_)] 
					for j_ in range(j, j+bloc)))

			if t+1 < len(blocs):
				
				binf2 = sum(blocs[:t+1]) + len(blocs[:t+1])
				bsup2 = nc - sum(blocs[t+1:]) - len(blocs[t+2:])

				for j in range(binf1, bsup1+1):
					end_of_y = min(j+blocs[t]+1, bsup2+1)

					if binf2 < end_of_y:
						m.addConstr(y_vars[(i,j,t+1)] \
							+ quicksum(y_vars[(i,j_,t+2)] 
								for j_ in range(binf2, end_of_y)) <= 1)


def set_z_vars(m, nl, c_blocks):
	"""Create z variables for each position that each block could take in the 
	columns.

	Args:
		m (Model): gurobi Model class.
		nl (int): number of lines.
		c_blocks (list list int): list of blocks for each column.

	Return: (dict) all variables indexed by their position in the grid and by
		their corresponding block number.
	"""

	z_vars = {}
	for j, blocs in enumerate(c_blocks):
		for t, bloc in enumerate(blocs):

			binf = sum(blocs[:t]) + len(blocs[:t])
			bsup = nl - sum(blocs[t:]) - len(blocs[t+1:])

			for i in range(binf, bsup+1):
				z_vars[(i,j,t+1)] = m.addVar(vtype=GRB.BINARY, 
					name="z[{},{},{}]".format(i,j,t+1))
				
	return z_vars


def set_z_constrs(m, nl, c_blocks, z_vars, x_vars):
	"""Set the constraints for each z_var.

	Args:
		m (Model): gurobi Model class.
		nl (int): number of lines.
		c_blocks (list list int): list of blocks for each column.
		z_vars (dict): collection of gurobi vars involved in the constraints.
		x_vars (dict): collection of gurobi vars involved in the constraints.
	"""

	for j, blocs in enumerate(c_blocks):
		for t, bloc in enumerate(blocs):
			binf1 = sum(blocs[:t]) + len(blocs[:t])
			bsup1 = nl - sum(blocs[t:]) - len(blocs[t+1:])

			m.addConstr(quicksum(z_vars[(i,j,t+1)] 
				for i in range(binf1, bsup1+1)) == 1)

			for i in range(binf1, bsup1+1):
				m.addConstr(bloc * z_vars[(i,j,t+1)] <= quicksum(x_vars[(i_,j)] 
					for i_ in range(i,i+bloc)))

			if t+1 < len(blocs):

				binf2 = sum(blocs[:t+1]) + len(blocs[:t+1])
				bsup2 = nl - sum(blocs[t+1:]) - len(blocs[t+2:])

				for i in range(binf1, bsup1+1):
					end_of_z = min(i+blocs[t]+1, bsup2+1)

					if binf2 < end_of_z:
						m.addConstr(z_vars[(i,j,t+1)] \
							+ quicksum(z_vars[(i_,j,t+2)] 
								for i_ in range(binf2, end_of_z)) <= 1)


def draw(x_vars, nl, nc, figname):
	x = [[int(x_vars[(i,j)].x) for j in range(nc)] for i in range(nl)]
	f = figure(1)
	im = imshow(x, interpolation='nearest', cmap='binary')
	# grid(True)
	savefig('report/images/{}.png'.format(figname))

if __name__ == "__main__":
	m = Model('picross')
	m.params.timelimit = 120

	instance = argv[1]
	l_blocks, c_blocks = parse_file('instances/{}.txt'.format(instance))
	nl = len(l_blocks) # number of lines
	nc = len(c_blocks) # number of columns

	print(nl, nc)

	x_vars = set_x_vars(m, nl, nc)
	y_vars = set_y_vars(m, nc, l_blocks)
	z_vars = set_z_vars(m, nl, c_blocks)

	set_y_constrs(m, nc, l_blocks, y_vars, x_vars)
	set_z_constrs(m, nl, c_blocks, z_vars, x_vars)

	m.setObjective(quicksum(x for x in x_vars.values()), GRB.MINIMIZE)
	m.optimize()

	if m.status == GRB.Status.OPTIMAL:
		draw(x_vars, nl, nc, '{}_plne'.format(instance))
	elif m.status == GRB.Status.INFEASIBLE:
		m.computeIIS()
	m.write("model.lp")