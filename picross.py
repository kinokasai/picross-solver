import pylab as pl
from pprint import PrettyPrinter
from utils import parse_file
from sys import argv
try:
	from functools import lru_cache
except ImportError:
	from repoze.lru import lru_cache

pp = PrettyPrinter()

COLORLESS = -1
BLACK = 1
WHITE = 0


def block_check(line, j, block_size):
	return WHITE not in line[j-block_size+1:j+1] and line[j-block_size] != BLACK


@lru_cache(maxsize=None)
def T(j, l, S, line):
	if l == 0:
		# Case l = 0 (1)
		# If no block to place, check that the cells are white or colorless.
		return BLACK not in line[:j+1]
	else:
		block_size = S[l-1]
		if j < block_size - 1:
			# Case (2a)
			return False
		elif j == block_size - 1:
			# Case (2b)
			return l == 1 and WHITE not in line[:j+1]

		# Case (2c)
		if line[j] == WHITE:
			return T(j - 1, l, S, line)
		elif line[j] == BLACK:
			return block_check(line, j, block_size) \
				and T(j - block_size - 1, l - 1, S, line)

		return T(j - 1, l, S, line) or (T(j - S[l-1] - 1, l - 1, S, line) and block_check(line, j, block_size))


def solve(M, l_blocks, c_blocks):
	nc = len(c_blocks)
	nl = len(l_blocks)

	lines_to_explore = list(range(nl))
	columns_to_explore = list(range(nc))

	while lines_to_explore or columns_to_explore:
		for i in lines_to_explore:
			for j in range(len(M[i])):
				if M[i][j] != COLORLESS: continue
				# copy line
				line_copy = [cell for cell in M[i]]
				# test white coloration
				line_copy[j] = WHITE
				can_be_white = T(len(line_copy)-1,len(l_blocks[i]),l_blocks[i],tuple(line_copy))
				# test black coloration
				line_copy[j] = BLACK
				can_be_black = T(len(line_copy)-1,len(l_blocks[i]),l_blocks[i],tuple(line_copy))

				# deduce color of cell
				if not (can_be_black or can_be_white):
					raise Exception()
				else:
					if can_be_white and not can_be_black:
						M[i][j] = WHITE
						if j not in columns_to_explore:
							columns_to_explore.append(j)
					elif can_be_black and not can_be_white:
						M[i][j] = BLACK
						if j not in columns_to_explore:
							columns_to_explore.append(j)

		lines_to_explore = []

		for j in columns_to_explore:
			for i in range(len(M)):
				if M[i][j] != COLORLESS: continue

				column_copy = [M[i_][j] for i_ in range(len(M))]
				column_copy[i] = WHITE
				can_be_white = T(len(column_copy)-1,len(c_blocks[j]),c_blocks[j],tuple(column_copy))
				column_copy[i] = BLACK
				can_be_black = T(len(column_copy)-1,len(c_blocks[j]),c_blocks[j],tuple(column_copy))

				if not (can_be_black or can_be_white):
					raise Exception()
				else:
					if can_be_white and not can_be_black:
						M[i][j] = WHITE
						if i not in lines_to_explore:
							lines_to_explore.append(i)
					elif can_be_black and not can_be_white:
						M[i][j] = BLACK
						if i not in lines_to_explore:
							lines_to_explore.append(i) 

		# import pdb; pdb.set_trace()
		columns_to_explore = []


def print_mat(M, instance):
	pl.imshow(M, interpolation='nearest', cmap='binary')
	# pl.show()
	pl.savefig('report/images/{}_dyn.png'.format(instance))


if __name__ == "__main__":
	instance = argv[1]
	l_blocks, c_blocks = parse_file('instances/{}.txt'.format(instance))
	nl = len(l_blocks)
	nc = len(c_blocks)

	M = [[COLORLESS for _ in range(nc)] for _ in range(nl)]
	solve(M, l_blocks, c_blocks)

	print_mat(M, instance)
