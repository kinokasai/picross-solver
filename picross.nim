import hashes
import times
import tables
import math
import options
import os
import sequtils
import strutils
import terminal
import zero_functional

type Unsolvable = object of Exception
var showtime = false
var showtime_speed = 50

proc flatten*[T](a: seq[seq[T]]): seq[T] =
  result = @[]
  for subseq in a:
    result &= subseq

#### Colors ####

type Color = enum open, white, black

proc to_sym(color : Color) : string =
  case color:
  of open:
    setBackgroundColor(bgBlue)
    result = " "
  of black:
    setBackgroundColor(bgBlack)
    result = " "
  of white:
    setBackgroundColor(bgWhite)
    result = " "

### Parallelism ###

type
  delta = object
    i : int
    color : Color

proc make_delta(i: int, color: Color) : delta =
  result = delta(i: i, color: color)

#### PICROSS #####

# A board is merely a conjunction of the color values and sequence arrays
type
  picross = object
    board : seq[seq[Color]]
    seqs: seq[seq[int]]
    # Commodity variables
    width: int 
    height: int

var pic: picross
    
#[proc init_picross(pic: var picross) =
  while pic.line_seq.len < pic.height:
    pic.line_seq.add(@[])
  while pic.col_seq.len < pic.width:
    pic.col_seq.add(@[])]#

proc parse_file(name: string) : (seq[seq[int]], seq[seq[int]]) =
  var f: File
  var tline: TaintedString
  var line_seq = newSeqWith(0, newSeq[int]())
  var col_seq = newSeqWith(0, newSeq[int]())
  type mode_enum = enum line, column
  var mode = line

  if f.open(name):
    try:
      while(f.readLine(tline)):
        var line = (string)tline
        if line == "#":
          mode = column
          continue
        var sequence = line.splitWhitespace()
        if mode == mode_enum.line:
          line_seq.add(sequence-->map(parseInt(it)))
        else:
          col_seq.add(sequence-->map(parseInt(it)))
    finally:
      result = (line_seq, col_seq)
      f.close()

proc newparse(name: string) : (seq[seq[int]], int) = 
  var f: File
  var tline: TaintedString
  result[0] = newSeqWith(0, newSeq[int]())
  if f.open(name):
    try:
      while(f.readLine(tline)):
        var line = (string)tline
        if line == "#":
          result[1] = result[0].len
        else:
          let sequence = line.splitWhitespace()
          result[0].add(sequence-->map(parseInt(it)))
    finally:
      f.close()


proc make_picross(filename: string) : picross =
  let (seqs, height) = newparse(filename)
  let width = seqs.len - height
  let board = newSeqWith(width, newSeq[Color](height))
  result = picross(board:board, seqs:seqs, width:width, height:height)
  # var (line_seq, col_seq) = parse_file(filename)
  # var board = newSeqWith(col_seq.len, newSeq[Color](line_seq.len))
  # result = picross(board:board, line_seq:line_seq, col_seq:col_seq, width: col_seq.len, height: line_seq.len)

# proc make_picross(line_seq, col_seq: seq[seq[int]]) : picross = 
#   var board = newSeqWith(col_seq.len, newSeq[Color](line_seq.len))
#   result = picross(board:board, line_seq:line_seq, col_seq:col_seq, width: col_seq.len, height: line_seq.len)

proc get_colseqs(pic: picross) : seq[seq[int]] =
  result = pic.seqs[pic.height..^1]

proc get_lineseqs(pic: picross) : seq[seq[int]] =
  result = pic.seqs[0..<pic.height]

proc get_line(pic: picross, i: int) : seq[Color] =
  result = @[]
  for s in pic.board:
    result &= s[i]

proc get_col(pic: picross, i: int) : seq[Color] =
  result = @[]
  result = pic.board[i]

proc col_seq_to_array(col_seq: seq[seq[int]]): seq[seq[string]] =
  var max_len = col_seq --> map(len(it)).fold(0, max(a, it))
  var col_seq = col_seq-->map(it-->map(toHex(it, 1)))
  var tmp_seq = newSeqWith(col_seq.len, newSeq[string](max_len))

  # Add padding so that each sequence is of same length
  for i in 0..<col_seq.len:
    var fillseq = newSeqWith(max_len - col_seq[i].len, " ")
    tmp_seq[i] = fillseq & col_seq[i]
  result = newSeqWith(max_len, newSeq[string](col_seq.len))
  for i in 0..<col_seq.len:
    var sequence = tmp_seq[i]
    for j in 0..<sequence.len:
      result[j][i] = tmp_seq[i][j]


proc draw(pic : picross) =
  terminal.resetAttributes()
  #setCursorPos(0, 0)
  let line_seq = pic.get_lineseqs()
  var max_len = line_seq --> map(len(it)).fold(0, max(a, it)) # Longest line sequence
  var offset = max_len * 3
  # Draw col_seq
  var cs_array = col_seq_to_array(pic.get_colseqs)
  for i in 0..<cs_array.len:
    stdout.write "\n" & " ".repeat(offset + 3)
    for j in 0..<cs_array[i].len:
      if showtime: sleep(showtime_speed)
      stdout.write cs_array[i][j]
      stdout.flushFile()

  terminal.resetAttributes()
  var board = pic.board
  # Start outerloop by ys
  for i in 0..<line_seq.len:
    let sequence = line_seq[i]
    stdout.write "\n"
    stdout.write(sequence-->map(it.toHex(1) & " "))
    stdout.write " ".repeat(offset - (sequence.len) * 2 + 3)#+ max(0, min(sequence.len, 1)) * 2)
    for j in 0..<pic.width:
      stdout.write board[j][i].to_sym
      terminal.resetAttributes()
      stdout.flushFile()
      if showtime: sleep(showtime_speed)
      terminal.resetAttributes()
  echo ""

#### MY PROC GODDDAMN

var debug = false

var cache = initTable[Hash, bool]()

proc Trec(j: int; seq_idx: int; line: seq[Color]; seqs_idx: int): bool =
  proc impl(j : int, seq_idx : int, line: seq[Color], seqs_idx: int) : bool =
    # let _ = _[i] assignments are to prevent the system from copying the seq.
    let subline = line[0..j]
    if seq_idx < 0:
      result = not (subline-->any(it == black)) # If sequence is empty, check that everything is either white or open
    elif j < 0:
      result = false # If no more cases remain available
    else:
      let block_size = pic.seqs[seqs_idx][seq_idx]
      if j < block_size - 1:
        result = false # Can't fit a block of n cases in n-1 cases
      elif j == block_size - 1:
        result = (subline-->all(it != white)) and # The block has to be made of black cases
                  seq_idx == 0 # Has to be the *last* block
      else:
        # let _ = _[i] assignments are to prevent the system from copying the seq.
        let color = line[j]
        let sub_block = line[j-block_size+1..j]
        let space_color = line[j-block_size]
        if color == black:
          result = (sub_block --> all(it != white)) and # The block has to be made of black cases
                    space_color != black            and # Whitespace has to be white or open
                    Trec(j - block_size - 1, seq_idx - 1, line, seqs_idx) # Check if available
        elif color == white:
            result = Trec(j - 1, seq_idx, line, seqs_idx) # Check if white
        else:
          result = ((sub_block --> all(it != white)) and # The block has to be made of black cases
                    space_color != black             and # Whitespace has to be white or open
                    Trec(j - block_size - 1, seq_idx - 1, line, seqs_idx)) or # Recurse on black
                    Trec(j - 1, seq_idx, line, seqs_idx) # Check if white

  # Memoisation
  # Store the hash of the seq instead of the seq itself to avoid copying
  let hpack = (j, seq_idx, line[0..j].hash, seqs_idx).hash # Access to [Hash] is faster than [tuple[]]
  if not cache.hasKey(hpack):
    result = impl(j, seq_idx, line, seqs_idx)
    cache[hpack] = result
  else:
    result = cache[hpack]

proc seqable(line: seq[Color], seq_idx: int) : bool =
  Trec(line.len - 1, pic.seqs[seq_idx].len - 1, line, seq_idx)

proc get_indexes(line : seq[Color]) : seq[int] {.inline.} =
  result = newSeq[int]()
  for i, color in line:
    if color == open:
      result.add(i)

proc compute_delta(line : seq[Color], seq_idx: int, idx: int) : Option[delta] =
  var line = line
  line[idx] = white
  var white = seqable(line, seq_idx)
  line[idx] = black
  var black = seqable(line, seq_idx)
  if black and white:
    result = none(delta)
  elif black:
    result = some make_delta(idx, Color.black)
  elif white:
    result = some make_delta(idx, Color.white)
  else:
    raise newException(Unsolvable, "Unsolvable puzzle.")

proc test_line(pic: var picross, line_idx : int) : seq[int] =
  result = newSeq[int]()
  var line = pic.get_line(line_idx)
  var open_idx = get_indexes(line)

  var deltas = newSeqWith(open_idx.len, none(delta))
  for i in 0..<open_idx.len:
    deltas[i] = compute_delta(line, line_idx, open_idx[i])
  deltas = deltas-->filter(isSome(it))
  for delta in deltas-->map(get(it)):
    pic.board[delta.i][line_idx] = delta.color
    result.add(delta.i)

proc test_col(pic: var picross, col_idx: int) : seq[int] =
  result = new_seq[int]()
  var col = pic.get_col(col_idx)
  let seq_idx = col_idx + pic.height
  var open_idx = get_indexes(col)
  var deltas = newSeqWith(open_idx.len, none(delta))
  for i in 0..<open_idx.len:
    deltas[i] = compute_delta(col, seq_idx, open_idx[i])
  deltas = deltas-->filter(isSome(it))
  for delta in deltas-->map(get(it)):
    pic.board[col_idx][delta.i] = delta.color
    result.add(delta.i)

var spaces = ""

proc tmploop(pic: var picross, lqueue, cqueue: var seq[int]) =
  while lqueue.len > 0 or cqueue.len  > 0:
    for idx in lqueue.deduplicate:
      var result = pic.test_line(idx)
      cqueue &= result
    if showtime: pic.draw()
    lqueue.setLen(0)
    for idx in cqueue.deduplicate():
      var result = pic.test_col(idx)
      lqueue &= result
    if showtime: pic.draw()
    cqueue.setLen(0)

proc desambiguate(pickle: picross, output: var picross) =
  if output.board != nil:
    return
  elif (pickle.board.flatten())-->all(it != open):
    output = pickle
  block loop:
    for i in 0..<pickle.width:
      for j in 0..<pickle.height:
        if pickle.board[i][j] == open:
          try:
            var tmpic = pickle
            tmpic.board[i][j] = white
            var lqueue = @[j]
            var cqueue = @[i]
            tmpic.tmploop(lqueue, cqueue)
            tmpic.desambiguate(output)
            break loop
          except Unsolvable:
            var tmpic = pickle
            tmpic.board[i][j] = black
            var lqueue = @[j]
            var cqueue = @[i]
            tmpic.tmploop(lqueue, cqueue)
            tmpic.desambiguate(output)
            break loop

proc colorify(pickle: var picross) =
  var lqueue = newSeq[int]()
  for i in 0..<pickle.height:
    lqueue.add(i)
  var cqueue = newSeq[int]()
  for i in 0..<pickle.width:
    cqueue.add(i)
  
  pickle.tmploop(lqueue, cqueue)
  var pic: picross
  pickle.desambiguate(pic)
  pickle = pic

template time(s: stmt): expr = 
  let t0 = cpuTime()
  s
  cpuTime() - t0

showtime = false
showtime_speed = 4
var print = false

if paramCount() < 2:
  echo "Please specify parameters. `./picross [print] instance"

var filename = if paramCount() > 1: paramStr(2) else: "instances/2.txt"
if paramStr(1) == "print":
  print = true

pic = make_picross(filename)
#eraseScreen()
var time_spent = 0.0
pic = make_picross(filename)
time_spent += time(pic.colorify())
var result = time_spent * 1000
echo result.round(2)
if print:
  pic.draw()