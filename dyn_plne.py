import pylab as pl
from sys import argv
from utils import parse_file
from picross import solve as dyn_solve
from gurobipy import Model, GRB
from plne import *
import time

COLORLESS = -1
BLACK = 1
WHITE = 0


if __name__ == "__main__":
	instance = argv[1]
	l_blocks, c_blocks = parse_file('instances/{}.txt'.format(instance))
	nl = len(l_blocks)
	nc = len(c_blocks)

	M = [[COLORLESS for _ in range(nc)] for _ in range(nl)]
	dyn_start = time.time()
	dyn_solve(M, l_blocks, c_blocks)
	print('dyn_solve_time: {}s'.format(time.time() - dyn_start))

	m = Model('picross')
	# m.params.timelimit = 120

	x_vars = set_x_vars(m, nl, nc)
	y_vars = set_y_vars(m, nc, l_blocks)
	z_vars = set_z_vars(m, nl, c_blocks)

	set_y_constrs(m, nc, l_blocks, y_vars, x_vars)
	set_z_constrs(m, nl, c_blocks, z_vars, x_vars)

	m.setObjective(quicksum(x for x in x_vars.values()), GRB.MINIMIZE)

	for i in range(nl):
		for j in range(nc):
			if M[i][j] != COLORLESS:
				x_vars[(i,j)].ub = M[i][j]
				x_vars[(i,j)].lb = M[i][j]

	m.optimize()

	if m.status == GRB.Status.OPTIMAL:
		draw(x_vars, nl, nc, '{}_dyn_plne'.format(instance))