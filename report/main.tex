\documentclass[a4paper, 10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage[margin=1in]{geometry}
\usepackage{hyperref}
\usepackage[french]{babel}
\usepackage[small, center]{titlesec}
\usepackage{listings}
\usepackage{float}
\usepackage{amsmath, amsthm, amssymb, mathtools}
\usepackage{subcaption}
\usepackage{array}
\usepackage[table]{xcolor}

\definecolor{light-gray}{gray}{0.985}

\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}}
\allowdisplaybreaks
\setlength{\parskip}{.5em}

\begin{document}
	\begin{center}
		\textbf{UE 4i200. Projet MOGPL.}\\
		\textbf{Un problème de tomographie discrète.}\\[0.5cm]
		\textit{Alexandre Bontems, Alexandre Doussot}
	\end{center}

	\section*{Partie 1 : Raisonnement par programmation dynamique.}

	Dans ce projet, on souhaite à partir d'une grille et d'une liste de contraintes appliquées aux lignes et colonnes de cette grille, trouver de manière optimale une coloration correspondant aux contraintes. On s'intéresse d'abord à un raisonnement par programmation dynamique.

	\paragraph{Première étape}{
		On donne le terme $T(j, l)$ qui, par définition, est vrai s'il est possible de colorier les $j+1$ premières cases d'une certaine ligne avec les blocs $(s_1, ..., s_l).$ Pour une ligne $l_i$ de $m$ cases avec $k$ blocs, $T(m-1, k)$ permet donc de déterminer s'il est possible de colorier la ligne.\\

	\noindent
	Dans une grille non coloriée, on calculera les $T(j, l)$ de la manière suivante:
	\begin{enumerate}
		\item $T(j, 0)$ est toujours vrai car on ne considère aucune contrainte; il sera possible de colorier les cases en blanc.
		\item Si $l \geq 1$ alors on a plusieurs cas possibles:
		\begin{enumerate}
			\item Si $j < s_l - 1$ alors on ne peut pas colorier les $j + 1$ premières cases en noir avec le dernier bloc et donc on renvoie faux.
			\item Si $j = s_l - 1$ alors on peut colorier les $j + 1$ premières cases avec uniquement le dernier bloc de la sous-séquence $(s_1, ..., s_l)$. Donc $T(j, l)$ est vrai uniquement si on ne considère qu'un seul bloc (si $l = 1$).
			\item Si $j > s_l - 1$ on pourra avoir en $j$ soit une case blanche soit une case noire. $T(j, l)$ est donc vrai si $T(j-1, l)$ est vrai, c'est-à-dire que la case $j$ est blanche et on peut colorier les cases précédentes avec les blocs $(s_1, ..., s_l)$. Ou si $T(j - s_l - 1, l - 1)$ est vrai, c'est-à-dire qu'on peut colorier les cases $j$ à $j - (s_l-1)$ en noir, la case $j - s_l$ en blanc et colorier les blocs $(s_1, ..., s_{l-1})$ dans les cases restantes. Comme la grille est non coloriée on renvoie la disjonction des deux récurrences.
		\begin{equation*}
			T(j-1, l) \text{ ou } T(j - s_l - 1, l - 1)
		\end{equation*}
		\end{enumerate}
	\end{enumerate}
	}

	\paragraph{Généralisation}{
		Dans l'algorithme de propagation que l'on souhaite implémenter, les grilles manipulées pourront être partiellement coloriées lorsqu'on voudra évaluer les $T(j, l)$. On va donc effectuer les mêmes assertions que précédemment mais en vérifiant la couleur des cases considérées:

		\begin{enumerate}
			\item $T(j, 0)$ est vrai si toutes les premières case $j + 1$ sont non coloriées ou blanches car on ne veut poser aucun bloc dans ces cases.
			\item Lorsque $l \geq 1$
			\begin{enumerate}
				\item Si $j < s_l - 1$ on renvoie faux pour la même raison que précédemment.
				\item Si $j = s_l - 1$ alors $T(j, l)$ renvoie vrai si toutes les cases précédentes sont non coloriées ou noires et $l = 1$.
				\item Lorsque $j > s_l - 1$:
				\begin{enumerate}
					\item\label{itm:casenoire} Si la case $j$ est coloriée en noir alors il faut vérifier que les cases $j$ à $j - s_l$ peuvent accueillir le bloc $s_l$; c'est-à-dire que les cases $j$ à $j - (s_l - 1)$ sont non coloriées ou noires, que la case $j - s_l$ est non coloriée ou blanche pour respecter l'intervalle minimum d'une case blanche entre blocs, et enfin que $T(j - s_l - 1, l - 1)$ est vrai pour être sûr que l'on peut poser les blocs précédents dans le reste de la ligne.
					\item \label{itm:caseblanche} Si la case $j$ est coloriée en blanc alors il faut simplement vérifier que $T(j - 1, l)$ renvoie vrai pour la même raison que précédemment dans une grille non coloriée.
					\item Si la case n'a pas de couleur affectée alors on doit vérifier que l'un des deux tests précédents est vrai et on renvoie la disjonction de~\ref{itm:casenoire} et~\ref{itm:caseblanche}.
				\end{enumerate}
			\end{enumerate}
		\end{enumerate}
	}

	\paragraph{Tests de l'algorithme de résolution}{
	L'algorithme de coloration donné en annexe a été implémenté en \texttt{nim}, un langage transpilé en \texttt{C}. Grâce à la mémoisation de la fonction $T$ et à la rapidité du \texttt{C}, on atteint les vitesses de résolution de la table~\ref{tab:timedyn}. La grille obtenue pour l'instance 9 est visible en figure~\ref{fig:instance9dyn}.

	\begin{table}[h!]
		\centering
    \begin{tabular}{P{2cm}|P{3.5cm}|P{3cm}|P{4cm}|}
    \cline{2-4}
    & \multicolumn{3}{|c|}{Temps moyen de résolution (en millisecs)} \\
    \hline
    \multicolumn{1}{|c|}{Instance} & gcc & clang & ucc \\
		\hline
		\multicolumn{1}{|c|}{1} & 0.19 & 0.17 & 0.22 \\
		\multicolumn{1}{|c|}{2} & 10.48 & 10.86 & 11.05 \\
		\multicolumn{1}{|c|}{3} & 8.95 & 9.18 & 9.09 \\
		\multicolumn{1}{|c|}{4} & 15.65 & 15.85 & 15.91 \\
		\multicolumn{1}{|c|}{5} & 21.33 & 21.75 & 22.2 \\
		\multicolumn{1}{|c|}{6} & 45.93 & 47.05 & 46.82 \\
		\multicolumn{1}{|c|}{7} & 27.94 & 28.59 & 28.49 \\
		\multicolumn{1}{|c|}{8} & 41.59 & 42.56 & 42.36 \\
		\multicolumn{1}{|c|}{9} & 429.1 & 434.89 & 433.17 \\
		\multicolumn{1}{|c|}{10} & 593.45 & 612.87 & 598.72 \\
    \hline

		\end{tabular}
		\caption{Temps de résolution pour l'algorithme de propagation}
		\label{tab:timedyn}
	\end{table}

	\begin{figure}[h!]
		\centering
		\includegraphics[width=0.7\textwidth]{images/9_dyn.png}
		\caption{Instance 9 obtenue via programmation dynamique}
		\label{fig:instance9dyn}
	\end{figure}

	\paragraph{Optimisation}
		Lors de la génèse de sa conception, le programme mettait plus d'une demi-heure à résoudre l'instance 9.
		L'optimisation réalisée la plus importante est la mémoïsation.
		Ensuite, il s'est surtout agit de réduire le nombre de copies réalisées par le langage.
		Nim étant compilé en C, il fut donc aisé d'étudier le code généré.

~\newline

	L'algorithme tel que présenté dans le sujet est incapable de résoudre l'instance \texttt{11.txt}
	parce que les blocs donnés ne sont pas assez contraignants pour permettre à l'algorithme de prendre une décision
	(colorier en blanc ou en noir) sur toutes les cases et la grille reste non coloriée.
	Pour pallier ce problème on va procéder à une modélisation en programmation linéaire en nombre entier:
	l'optimisation d'une fonction objectif permettra d'obtenir une coloration même en cas d'indécision comme dans cette instance.
	Nous proposons également une adaptation de l'algorithme susmentionné afin de résoudre de telles situations. Celle-ci est décrite
	dans la partie 3.


	\section*{Partie 2 : La PLNE à la rescousse}

	\paragraph{Modélisation}{
		On pose une variable $x_{ij}$ pour chacune des cases de la grille, donc $N \times M$ variables qui prendront la valeur 0 si la case est coloriée en blanc et 1 si la case est coloriée en noire. On pose également les variables $y^t_{ij}$ qui valent 1 si le bloc numéro $t$ de la ligne $l_i$ commence à la case $(i, j)$ et 0 sinon. De même, les variables $z^t_{ij}$ valent 1 si le bloc numéro $t$ de la colonne $c_j$ commence à la case $(i,j)$ et 0 sinon.

		\noindent
		Ce problème est alors soumis à 3 types de contraintes:
		\begin{enumerate}
			\item Si le bloc numéro $t$ de la ligne $l_i$ commence à la case $(i,j)$ alors les cases de $(i,j)$ à $(i,j+s_t-1)$ doivent être coloriées en noir. On veut donc:
			\begin{equation*}
				\sum_{k=j}^{j+s_t-1} x_{ik} = s_t
			\end{equation*}
			On aimerait que lorsque la variable $y$ d'un bloc prend la valeur 1, les $x$ correspondant prennent également la valeur 1. Ainsi, pour chaque case $(i,j)$ à laquelle un bloc d'indice $t$ est susceptible de commencer dans la ligne $l_i$, on écrira la contrainte de l'équation~\ref{constr:1}. Ainsi si la variable $y^t_{ij}$ est active, tous les $x_{ij}$ concernés doivent prendre la valeur 1 pour que la contrainte soit respectée. Sinon les $x_{ij}$ peuvent prendre n'importe quelle valeur.

%			\begin{minipage}{0.5\linewidth}
				\begin{equation}
					s_t \times y^t_{ij} \leq \sum_{k=j}^{j+s_t-1} x_{ik}
					\label{constr:1}
				\end{equation}
%			\end{minipage}%
%			\begin{minipage}{0.5\linewidth}
				\begin{equation}
					st \times z^t_{ij} \leq \sum_{k=i}^{i+s_t-1} x_{kj}
					\label{constr:2}
				\end{equation}
%			\end{minipage}

			En suivant le même raisonnement en colonne, pour chaque case $(i,j)$ à laquelle un bloc d'indice $t$ est susceptible de commencer dans la colonne $c_j$, on écrira la contrainte de l'équation~\ref{constr:2}.
			\item Si le bloc numéro $t$ de la ligne $l_i$ commence à la case $(i,j)$ alors le bloc suivant ne peut commencer avant la case $(i,j+s_t+1)$. Pour chaque case $(i,j)$ à laquelle un bloc d'indice $t$ est susceptible de commencer dans la ligne $l_i$ on va écrire les contraintes suivantes:
			\begin{equation*}
				y^t_{ij} + \sum_{j^\prime=0}^{j+s_t+1} y^{t+1}_{ij^\prime} \leq 1
			\end{equation*}
			On s'assure ainsi qu'on ne puisse pas avoir de blocs qui se chevauchent, que la séparation d'au moins une case blanche est respectée, et que l'ordre des blocs est respecté. La même contrainte en colonne se formule ainsi:
			\begin{equation*}
				z^t_{ij} + \sum_{i^\prime=0}^{i+s_t+1} z^{t+1}_{i^\prime j} \leq 1
			\end{equation*}

			\item Enfin pour que tous les blocs apparaissent correctement dans la coloration, il faut vérifier qu'ils apparaissent tous exactement une fois dans leur ligne et colonne respective.
			\begin{align*}
				\forall t \text{ de la ligne }l_i, \sum_j y^t_{ij} = 1 \\
				\forall t \text{ de la colonne }c_j, \sum_i z^t_{ij} = 1
			\end{align*}
		\end{enumerate}

		En ce qui concerne la fonction objectif, on se contentera de chercher le minimum de $x_{ij}$ à 1 et les contraintes assureront le bon nombre de cases noires. On arrive au problème d'optimisation suivant.

		\begin{gather*}
			\min \quad \sum_{i=0}^{N-1} \sum_{j=0}^{M-1} x_{ij} \\
			\begin{aligned}
			\textup{\textbf{s.c.}}\quad
         		\forall i, \forall t \text{ de la ligne }l_i, \quad \sum_j y^t_{ij} &= 1 \\
				\forall j, \forall t \text{ de la colonne }c_j, \quad \sum_i z^t_{ij} &= 1 \\
				\forall y^t_{ij}, \quad s_t \times y^t_{ij} \leq \sum_{k=j}^{j+s_t-1} x_{ik} \\
				y^t_{ij} + \sum_{j^\prime=0}^{j+s_t+1} y^{t+1}_{ij^\prime} \leq 1 \\
				\forall z^t_{ij}, \quad st \times z^t_{ij} \leq \sum_{k=i}^{i+s_t-1} x_{kj} \\
				z^t_{ij} + \sum_{i^\prime=0}^{i+s_t+1} z^{t+1}_{i^\prime j} \leq 1
			\end{aligned}
		\end{gather*}
	}

	\paragraph{Implémentation et tests}{
		On se rend compte que dans une ligne $l_i$, il existe un intervalle pour chaque bloc hors duquel ils ne peuvent pas commencer. Il faut en effet prendre en compte la taille des blocs précédents et l'intervalle minimum d'une case blanche entre chaque bloc. Ainsi pour un bloc $s_t$ donné, il ne sera nécéssaire de créer que les variables suivantes:
	\begin{equation*}
		\left\{ y^t_{i,j} | j \in \left\{ b_{inf}, ..., b_{sup} \right\} \right\}
	\end{equation*}
	Et soit $M$ le nombre de cases dans la ligne, et $k$ le nombre total de blocs pour la ligne.
	\begin{align*}
		b_{inf} &= \sum_{t^\prime=1}^{t-1} s_{t^\prime} + \mathbf{card}(s_1, ..., s_{t-1}) \\
		b_{sup} &= M - \sum_{t^\prime=t}^{k} s_{t^\prime} - \mathbf{card}(s_{t+1}, ..., s_k)
	\end{align*}
	On peut bien sûr appliquer le même traitement pour les variables $z$.

	\begin{figure}[h!]
		\centering
		\includegraphics[width=0.5\textwidth]{images/11_plne.png}
		\caption{Instance 11 obtenue via \texttt{gurobi}}
		\label{fig:instance11}
	\end{figure}

	La résolution du problème se fait grâce au logiciel d'optimisation \texttt{gurobi}; les temps de résolution sont visibles en table~\ref{tab:timeplne}. Bien qu'on soit ainsi capable de résoudre n'importe quel type d'instance (voir figure~\ref{fig:instance11} pour résolution de l'instance 11), les temps de calcul apparaissent bien supérieurs à ceux de l'algorithme de programmation dynamique. En effet la plupart des grandes instances semblent ne pas être résolvables en moins de deux minutes.

	\begin{table}[h!]
		\centering
		\begin{tabular}{P{2cm}|P{3cm}|P{3.5cm}|P{4cm}}
			Instance & Taille de la grille & Nombre de contraintes linéaires & Temps d'exécution (secs) \\
			\hline
			0 & $4 \times 5$ & 39 & 0.02 \\
			1 & $5 \times 5$ & 72 & 0.01 \\
			2 & $20 \times 20$ & 2026 & 6.18 \\
			3 & $13 \times 37$ & 2123 & 1.30 \\
			4 & $25 \times 25$ & 3678 & 178.05 \texttt{(TIMEOUT)} \\
			5 & $25 \times 27$ & 2619 & 156.17 \texttt{(TIMEOUT)} \\
			6 & $30 \times 30$ & 5626 & 789.45 \texttt{(TIMEOUT)} \\
			7 & $31 \times 34$ & 3853 & 48.84 \\
			8 & $40 \times 35$ & 5156 & 310.52 \texttt{(TIMEOUT)} \\
			9 & $50 \times 50$ & 23095 & \texttt{TIMEOUT} \\
			10 & $99 \times 99$ & 45671 & \texttt{TIMEOUT} \\
			11 & $2 \times 4$ & 23 & 0.01 \\
			12 & $33 \times 28$ & 4874 & 760.61 \texttt{(TIMEOUT)} \\
			13 & $45 \times 45$ & 8863 & 29.70 \\
			14 & $30 \times 38$ & 4669 & \texttt{TIMEOUT} \\
			15 & $30 \times 30$ & 4127 & \texttt{TIMEOUT} \\
			16 & $35 \times 50$ & 11217 & \texttt{TIMEOUT}
		\end{tabular}
		\caption{Temps de résolution par \texttt{gurobi}}
		\label{tab:timeplne}
	\end{table}
	}

	\clearpage
	\section*{Partie 3 : Pour aller plus loin}

	\paragraph{Prétraitement avec programmation dynamique}{
		Au vu des pauvres performances de \texttt{gurobi} sur notre problème, on se propose d'appliquer d'abord la programmation dynamique puis de résoudre le programme linéaire seulement pour les cases indéterminées.

		\begin{table}[h!]
		\centering
		\begin{tabular}{P{2cm}|P{3.5cm}|P{3cm}|P{4cm}}
			Instance & Prétraitement dynamique (secs) & \texttt{gurobi} (secs) & Temps total (secs) \\
			\hline
			12 & 0.84 & 0.16 & 1 \\
			13 & 1.26 & 0.04 & 1.30 \\
			14 & 0.64 & 0.05 & 0.69 \\
			15 & 0.70 & 30.88 & 31.58 \\
			16 & 3.03 &  & \texttt{TIMEOUT}
		\end{tabular}
		\caption{Temps de résolution par \texttt{picross.py} + \texttt{gurobi}}
		\label{tab:timemix}
	\end{table}
	}

	\paragraph{Résolution avec programmation dynamique uniquement}{}

	Pour pallier aux problèmes de performances de gurobi, on se propose d'adapter l'algorithme de programmation dynamique
	afin de résoudre les instances 11 à 16. Ces instances se démarquent des précédentes par le fait qu'elle proposent à un moment
	de leur résolution une situation non-décidable - ce qui fait qu'on les qualifiera d'ambigues. Pour pallier à cette situation,
	on se propose d'utiliser un algorithme de force brute se basant sur le principe suivant:

	Une case est soit blanche, soit noire. Ainsi, on force la couleur d'une case à blanc. Si l'on se retrouve avec un problème irrésolvable,
	alors c'est que la case était noire. On recommence alors en forçant la couleur de la dite case à noir.
	On obtient alors une complexité pire cas de $O(2^n)$, n étant le nombre de cases \textit{ouvertes}.

	On peut de prime abord supposer que cette solution est non-efficace. Cependant, le nombre de cases indécidables n'est pas de si grande envergure
	sur les instances proposées - forcer la décision d'une case entraine la décision d'un certain nombre de ses voisines. De plus, les performances
	de la version avec programmation dynamique sont de telle façon supérieures que ce coût est finalement dérisoire.

  \begin{table}[h!]
  \centering
  \rowcolors{1}{white}{light-gray}
    \begin{tabular}{P{2cm}|P{3.5cm}|P{3cm}|P{4cm}|}
    \cline{2-4}
  \hiderowcolors
    & \multicolumn{3}{c|}{Temps moyen de résolution (en millisecs)} \\
    \hline
    \showrowcolors
    \multicolumn{1}{|c|}{Instance} & gcc & clang & ucc \\
		\hline
		\multicolumn{1}{|c|}{11} & 0.06 & 0.08 & 0.07 \\
		\multicolumn{1}{|c|}{12} & 46.99 & 47.26 & 48.16 \\
		\multicolumn{1}{|c|}{13} & 47.12 & 49.05 & 47.94 \\
		\multicolumn{1}{|c|}{14} & 43.3 & 43.58 & 44.05 \\
		\multicolumn{1}{|c|}{15} & 38.7 & 39.51 & 39.38 \\
		\multicolumn{1}{|c|}{16} & 283.51 & 270.15 & 284.48 \\
    \hline

	\end{tabular}
  \caption{Temps de résolution par \texttt{picross.nim} en millisecondes.}
\end{table}

\end{document}
